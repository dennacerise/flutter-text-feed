import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget buildCard(String imageUrl) {
    return Card(
      semanticContainer: true,
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Text(
          text,
          textScaleFactor: 1.5,
        ),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    final title = "Notes";

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: new AppBar(
          title: new Text("My Notes"),
          backgroundColor: Colors.orange,
        ),
        body: ListView(
          children: [
            buildCard("Hello there."),
            buildCard("What do you want to say?"),
          ],
        ),
      ),
    );
  }

}
